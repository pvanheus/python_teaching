\documentclass[a4paper]{beamer}

\usepackage{hyperref}
\usepackage{courier}
\usepackage{listings}
\lstset{language=Python,showstringspaces=false,basicstyle=\tiny\ttfamily}
\usepackage{color}
\definecolor{codebg}{gray}{0.90}
\usepackage{caption}
\captionsetup[lstlisting]{font=scriptsize}
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=gray,urlcolor=links}
\usepackage{pgf}
\usepackage{tabularx}

\begin{document}

\begin{frame}
\frametitle{Introduction to Biopython}
\begin{itemize}
\item ``The Biopython project is a mature open source international collaboration of volunteer developers, providing Python libraries for a wide range of bioinformatics problems.'' --- \href{http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2682512/}{Biopython paper}
\pause
\item Available for Linux, Windows and MacOS X, either as a download or via package manager (e.g. \texttt{apt-get install python-biopython} on Ubuntu)
\pause
\item Excellent \href{http://biopython.org/DIST/docs/tutorial/Tutorial.html}{tutorial} on \href{http://biopython.org/wiki/Main_Page}{Biopython website}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The Biopython Seq object}

\begin{itemize}
\item A nucleotide or amino acid sequence in Biopython is modelled using the \lstinline|Bio.Seq.Seq| class: \lstinline|Bio.Seq| is the package name and \lstinline|Seq| is the package name
\pause
\item The optional \lstinline|alphabet| parameter constrains the operations that can be performed on the sequence to ones appropriate for the sequence type
\pause
\item In some respects the \lstinline|Seq| object operates like a string with some string-like methods
\end{itemize}
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={seq1.py},linewidth=1.0\textwidth]{src/biopython/seq1.py}

\end{frame}

\begin{frame}[fragile]
\frametitle{More on Seq objects}
\begin{itemize}
\item Like strings, \lstinline|Bio.Seq.Seq| objects are immutable (so each method returns a new object)
\pause
\item Operators and methods:
\end{itemize}
\begin{tabularx}{\textwidth}{lX}
\lstinline|mystring in myseq| & True if \lstinline|mystring| is contained in \lstinline|myseq| (the query can be either a \lstinline|Seq| object or a string) \\
\lstinline|myseq.count(subseq)| & Returns the number of occurences of \lstinline|subseq| in \lstinline|myseq| \\
\lstinline|myseq.complement()| & Returns the complementary DNA (or RNA) sequence \\
\lstinline|myseq.reverse_complement()| & Returns the reversed complement of the sequence \\
\end{tabularx}
\end{frame}

\begin{frame}[fragile]
\frametitle{Bio.Seq.Seq operators and methods (cont)}
\begin{tabularx}{\textwidth}{lX}
\lstinline|myseq.transcribe()| & Return a RNA sequence equivalent to the DNA sequence \\
\lstinline|myseq.back_transcribe()| & Returns a RNA sequence equivalent to the DNA sequence \\
\lstinline|myseq.translate()| & Translates DNA or RNA into protein \\
\lstinline|myseq.ungap()|& Returns a \lstinline|Seq| object with gap characters removed.\\
\end{tabularx}
See the \href{http://biopython.org/DIST/docs/api/Bio.Seq.Seq-class.html}{documentation} for details.
\end{frame}

\begin{frame}[fragile]
\frametitle{Sequences and Alphabets}
\begin{itemize}
\item Defining a sequence's alphabet allows for sanity checking operations on sequences, e.g.
\end{itemize}
\pause
\begin{lstlisting}
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

dna = Seq('GATACCAGC', IUPAC.unambiguous_dna)
protein = Seq('DTS', IUPAC.protein)
seq = dna + protein
\end{lstlisting}
\pause
\begin{itemize}
\item This will result in a \lstinline|TypeError| exception
\pause
\item This sanity checking has limits though, and the following code will work:
\end{itemize}
\pause
\begin{lstlisting}
seq = Seq('ThisIsNotANucleotideSeq', IUPAC.unambiguous_dna)
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{More about Alphabets}
\begin{itemize}
\item The \lstinline|Bio.Alphabet| class and its sub-classes set up the types of alphabet in Biopython
\pause
\item Standard alphabets are defined in \lstinline|Bio.Alphabet.IUPAC| and include:
\end{itemize}
\begin{tabularx}{\textwidth}{lX}
IUPAC.unambiguous\_dna & DNA as A,C,T,G\\
IUPAC.unambiguous\_rna & RNA as A,C,U,G\\
IUPAC.ambiguous\_dna & DNA include all ambiguous base characters: R,Y,W,S,M,K,H,B,V,D and N\\
IUPAC.ambiguous\_rna & RNA including the ambiguous base characters\\
IUPAC.extended\_dna & DNA including the non-standard bases B, D, S and W\\
IUPAC.protein & Standard amino acid alphabet \\
IUPAC.extended\_protein & Amino acids including rare or non-standard ones \\
\end{tabularx}
\end{frame}

\begin{frame}[fragile]
\frametitle{Even more on Alphabets}
\begin{itemize}
\item Each alphabet object has a \lstinline|letters| attribute containing its list of letters:
\end{itemize}
\begin{lstlisting}
from Bio.Alphabet import IUPAC

alpha = IUPAC.unambiguous_dna
print alpha.letters
\end{lstlisting}
\pause
\begin{itemize}
\item We can use a \lstinline|Seq| object's \textbf{alphabet} attribute to get or set its alphabet:
\end{itemize}
\begin{lstlisting}
myseq = Bio.Seq.Seq('GATAAC', alphabet=IUPAC.unambiguous_dna)
myalpha = myseq.alphabet
myseq.alphabet = IUPAC.protein
\end{lstlisting}
\pause
\begin{itemize}
\item We can create a gapped alphabet from any ungapped alphabet object:
\end{itemize}
\begin{lstlisting}
import Bio.Alphabet
alpha = IUPAC.protein
gapped_alpha = Bio.Alphabet.Gapped(alpha)
print gapped_alpha.gap_char
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{A note on comparing Seq objects}
\begin{itemize}
\item You cannot use the \textbf{==} operator to compare \lstinline|Bio.Seq.Seq| objects
\pause
\item This is because \textbf{==} would compare to see if the two variables refer to the same sequence objects
\pause
\item Compare the sequence string instead:
\end{itemize}
\begin{lstlisting}
myseq1 = Seq('GAT')
myseq2 = Seq('GAT')
# don't do this:
if myseq1 == myseq2:
    print "The same sequence!"
# do this
if str(myseq1) == str(myseq2):
    print "The same sequence!"
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{MutableSeq objects}
\begin{itemize}
\item \lstinline|Bio.Seq.MutableSeq| objects are like \lstinline|Bio.Seq.Seq| objects, except they are mutable
\pause
\item Because they are mutable, methods that operate them (e.g. \textbf{complement()}) operate ``in place'' and do not return a value
\pause
\item The \textbf{tomutable()} method of the \lstinline|Bio.Seq.Seq| class returns a mutable sequence, and similarly the \textbf{toseq()} method of the \lstinline|Bio.Seq.MutableSeq| class returns an immutable sequence
\end{itemize}
\pause
\begin{lstlisting}
from Bio.Seq import Seq, MutableSeq

seq1 = Seq('GATAACA')
mut_seq = seq1.tomutable()
mut_seq[3] = 'T'

rc_seq = seq1.reverse_complement()
mut_seq.reverse_complement() # in place transform

mut_seq2 = MutableSeq('GATAACA')
seq2 = mut_seq2.toseq()
\end{lstlisting}
\end{frame}

\end{document}
