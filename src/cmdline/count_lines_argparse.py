#!/usr/bin/python

import argparse

parser = argparse.ArgumentParser(description='Count the lines in a file')
parser.add_argument('input_filename', help='Input file')
args = parser.parse_args()

input_file = open(args.input_filename)
count = 0
for line in input_file:
    count += 1
print "line count:", count