#!/usr/bin/python

import argparse

parser = argparse.ArgumentParser(description='Read the specified line from a file')
parser.add_argument('--zero_based',
                    default=False, action='store_true',
                    help='Line numbers start from zero')
parser.add_argument('line_num', type=int, help='The line number to read')
parser.add_argument('in_file', type=argparse.FileType(), help='Input file')
parser.add_argument('out_file', type=argparse.FileType('w'), help='Out file')
args = parser.parse_args()

if args.zero_based:
    count = 0
else:
    count = 1

for line in args.in_file:
    if count == args.line_num:
        args.out_file.write(line)
        break
    count += 1

args.out_file.close()
args.in_file.close()