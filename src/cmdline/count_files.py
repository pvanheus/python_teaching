#!/usr/bin/python

import argparse

parser = argparse.ArgumentParser(description='Count the lines in a list of files')
parser.add_argument('input_files', nargs='*', type=argparse.FileType(), 
                    help='Files to read')
args = parser.parse_args()

total_lines = 0
for in_file in args.input_files:
    file_line_count = 0
    for line in in_file:
        total_lines += 1
        file_line_count += 1
    print in_file.name, "has", file_line_count, "lines"
print "total lines:", total_lines