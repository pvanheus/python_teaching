#!/usr/bin/python

import sys

if len(sys.argv) != 2:
    sys.stderr.write('Usage: %s <filename>\n' % (sys.argv[0]))
    sys.exit(1)
input_filename = open(sys.argv[1])
input_file = open(input_filename)
count = 0
for line in input_file:
    count += 1
print "line count:", count