\documentclass[a4paper]{beamer}

\usepackage{hyperref}
\usepackage{courier}
\usepackage{listings}
\lstset{language=Python,showstringspaces=false,basicstyle=\tiny\ttfamily}
\usepackage{color}
\definecolor{codebg}{gray}{0.90}
\usepackage{caption}
\captionsetup[lstlisting]{font=scriptsize}
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=gray,urlcolor=links}
\usepackage{pgf}

\begin{document}

\begin{frame}
\frametitle{Linux processes}
\begin{itemize}
\item Each process is identified by a numerical Process ID (PID)
\pause
\item Processes call on kernel services using system calls
\pause
\item They can inherit environment (environment variables and open files) from their parent processes
\pause
\item On exit the process provides a return code to the parent process
\pause
\item For the technically minded, see \href{http://bit.ly/unixprocesses}{process creation in Unix}
\end{itemize}
\end{frame}


\pgfdeclareimage[width=5cm]{process1}{images/unix_parent_child1.png}
\pgfdeclareimage[width=5cm]{process2}{images/unix_parent_child2.png}
\pgfdeclareimage[width=5cm]{process3}{images/unix_parent_child_full.png}
\begin{frame}[fragile]
\frametitle{Creating processes}
\pgfuseimage{process1}<1>
\pgfuseimage{process2}<2>
\pgfuseimage{process3}<3>
\end{frame}

\begin{frame}
\frametitle{The subprocess module}
The subprocess module provides an interface to creating new processes and handles three major concerns:
\begin{itemize}
\item Passing parameters to child processes
\item Dealing with child processes that exit and their return values
\item Communicating with child processes
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{A first example}
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_call.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_call.py}
Output:
\begin{footnotesize}
\begin{verbatim}
total 4
-rwxr-xr-x 1 pvh pvh 92 Mar 13 00:01 subprocess_call.py
\end{verbatim}
\end{footnotesize}
\end{frame}

\begin{frame}[fragile]
\frametitle{What does .call() do?}
\begin{itemize}
\item The \lstinline|subprocess.call()| function allows your Python script to run a program
\pause
\item Your Python script pauses while waiting for the child process created by \lstinline|subprocess.call()| to stop
\pause
\item When \lstinline|shell=True|, \lstinline|subprocess.call| passes the command to the shell to be interpreted. This is \textbf{dangerous!}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Return codes, exit codes and signals}
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_call\_exitcode.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_call_exitcode.py}
\begin{itemize}
\item The value returned from \lstinline|subprocess.call()| contains the exit code of the child process
\pause
\item If the child terminated because it received a signal, the return code is 128+N, where N is the number of the signal that the child process received (e.g. 15 for the SIGTERM signal that the \textbf{kill} command sends by default)
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Commands as lists: shell=False}
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_call\_noshell.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_call_noshell.py}
\begin{itemize}
\item By default \lstinline|shell=False|. This means that the command is not being passed as a string to the shell (which is good from the point of view of safety)
\pause
\item If \lstinline|shell=False| the command must be a list: the command to run followed by all of its arguments
\pause
\item The \lstinline|shlex| module provides a function \lstinline|shlex.split()| for splitting a command line string into a list, just like a shell would
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Limitations of .call()}
\begin{itemize}
\item Our script is paused while the command started with \lstinline|subprocess.call()| is running
\pause
\item This means that:
\begin{itemize}
\item While we can redirect the output of the command we're running so that it can be read by the parent Python script, its not safe to do so
\pause
\item We can only run one command at a time (we can't work in parallel)
\end{itemize}
\pause
\item Both of these limitations are addressed by the \lstinline|subprocess.Popen()| class
\end{itemize}
\end{frame}

\pgfdeclareimage[width=0.4\textwidth]{buffer}{images/buffer.png}
\pgfdeclareimage[width=0.4\textwidth]{two_way_buffer}{images/two_way_buffer.png}
\begin{frame}
\frametitle{The danger with inter-process I/O}
\pgfuseimage{buffer}
\begin{itemize}
\item When a process writes to a file, its output is first placed in an in-memory queue called a buffer
\pause
\item The buffer has a fixed size, when it is full the write function blocks until there is free space in the buffer
\pause
\item If the other end of the buffer is itself waiting on the process writing into the buffer \textbf{deadlock} occurs
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The Popen class}
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_popen.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_popen.py}
\begin{itemize}
\item The call to \lstinline|subprocess.Popen()| creates a new \lstinline|Popen| object (\textbf{proc} in the example)
\pause
\item Both \lstinline|.call()| and \lstinline|Popen()| accept \lstinline|stdin=|, \lstinline|stdout=| and \lstinline|stderr=| parameters to direct how to ``connect'' the standard file descriptors, but only the \lstinline|Popen()| ones are safe
\pause
\item The \lstinline|Popen()| call starts the specified command and return immediately. The \lstinline|wait()| method on the \lstinline|Popen()| object will wait for the subprocess to complete and return the return code
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Communicating with a Popen object}
\begin{itemize}
\item If a \lstinline|Popen()| call sets up \lstinline|stdin|, \lstinline|stdout| or \lstinline|stderr| as \lstinline|subprocess.PIPE|, the parent process can read/write to the child process
\pause
\item The \lstinline|Popen()| object has \lstinline|.stdin|, \lstinline|.stdout| and \lstinline|.stderr| attributes that refer to the standard file objects on the child process
\end{itemize}
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={pipe\_write\_read.py},linewidth=1.0\textwidth]{src/subprocess/pipe_write_read.py}
\end{frame}

\begin{frame}[fragile]
\frametitle{Redirecting I/O with Popen}
\begin{itemize}
\item The \lstinline|stdin|, \lstinline|stdout| and \lstinline|stderr| files can be redirected by passing open file objects in the \lstinline|Popen()| call
\pause
\item This is similar to the way redirection in the \textbf{bash} shell works: the child process thinks it is reading/writing the standard file descriptors, but they are connected to files, instead of to the terminal or to the parent process
\pause
\item The special value \lstinline|subprocess.STDOUT| can be used to connect \lstinline|stderr| to wherever \lstinline|stdout| is connected
\end{itemize}
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_redirect.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_redirect.py}
\end{frame}

\begin{frame}[fragile]
\frametitle{Communicating using the communicate method}
\begin{itemize}
\item As was illustrated in the \lstinline|subprocess.PIPE| example, \textbf{deadlock} can easily occur if communication with a child process is not handled correctly
\pause
\item The \lstinline|communicate()| method can be used to manage sending and receiving data from a child process
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_communicate.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_communicate.py}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Starting multiple subprocesses}
\begin{itemize}
\item Because \lstinline|Popen()| starts	the subprocess running immediately, it can be used to run more than one process at the same time
\end{itemize}
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={subprocess\_blasts.py},linewidth=1.0\textwidth]{src/subprocess/subprocess_blasts.py}
\end{frame}

\begin{frame}[fragile]
\frametitle{Conclusion}
\begin{itemize}
\item A Python process can create a subprocess that:
\pause
\begin{itemize}
\item Can inherit open files from the parent process (the Python script)
\pause
\item Returns a return code to the parent process when it exits
\end{itemize}
\pause
\item The \lstinline|subprocess| module allows for the simple creation of subprocesses via the \lstinline|call| function
\pause
\item The subprocess can either be specified via a command line string, or (preferably) via a list of command and arguments
\pause
\item To gain more flexibility in handling child process I/O and starting more than one subprocess at a time, use \lstinline|Popen()| to create subprocess objects
\pause
\item Pipes make communication between parent and child process possible. This needs to be set up when the subprocess is created (in the arguments of the \lstinline|Popen()| call)
\pause
\item As soon as a subprocess is created with \lstinline|Popen()| it starts running. This allows starting multiple child processes in parallel

\end{itemize}
\end{frame}

\end{document}
