\documentclass[a4paper]{beamer}

\usepackage{hyperref}
\usepackage{courier}
\usepackage{listings}
\lstset{language=Python,showstringspaces=false,basicstyle=\tiny\ttfamily}
\usepackage{color}
\definecolor{codebg}{gray}{0.90}
\usepackage{caption}
\captionsetup[lstlisting]{font=scriptsize}
%\printanswers

\begin{document}

\begin{frame}
\frametitle{Command line arguments}
\begin{itemize}
\item When a Python script runs it gets a list of command line arguments
\pause
\item Command line arguments allow for more than `default' behaviour for a script
\pause
\item The \lstinline|sys| module provides access to that list as \lstinline|sys.argv|
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{The sys.argv list}
\begin{lstlisting}[backgroundcolor=\color{codebg},linewidth=0.8\textwidth]
#!/usr/bin/python

import sys

print "this script is:", sys.argv[0]
print "my command line is:", sys.argv[1:]
\end{lstlisting}
\begin{itemize}
\item The name \lstinline|argv| originally meant argument vector.
\pause
\item The first item of the \lstinline|sys.argv| list is the name of the running program (the script).
\pause
\item The command line arguments (if any) start from \lstinline|sys.argv[1:]| onwards.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Reading arguments from sys.argv}
\begin{itemize}
\item If we have made our script runnable by including the \lstinline|#!/usr/bin/python| line
\pause
\item and setting execute permissions with \textbf{chmod a+x}
\pause
\item we can count the number of command line arguments we are given, it is \lstinline|len(sys.argv) - 1|
\pause
\item and we can read them to give our program parameters (see next slide).
\pause
\item Command line arguments are to a program what parameters are to a function: they are pieces of data to work with
\pause
\item but they are only strings (no lists or files or dictionaries).
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Count lines of a file}
\lstinputlisting[backgroundcolor=\color{codebg},title={count\_lines\_cmdline.py},linewidth=0.8\textwidth]{src/cmdline/count_lines_cmdline.py}
\pause
\begin{itemize}
\item If we're using \lstinline|sys.argv|, we need to check its length, else we might get a \lstinline|IndexError|
\pause
\item If we don't have the right command line arguments we tell the user (through a message written to \lstinline|sys.stderr|) and exit straight away (using \lstinline|sys.exit|)
\pause
\item The \lstinline|sys.exit| function takes an ``exit code'' as its single parameter. By convention, an exit code of $0$ means everything completed ok, any other number shows that an error occurred.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{When in doubt: use argparse}
\begin{itemize}
\item Handling \lstinline|sys.argv| ourselves is fine for simple cases, but since command line arguments are a common part of programs, Python provides several modules for dealing with them.
\pause
\item Unless you're using Python older than version 2.7, the best module to use is \lstinline|argparse|
\end{itemize}
\pause
\lstinputlisting[backgroundcolor=\color{codebg},title={count\_lines\_argparse.py},linewidth=0.9\textwidth]{src/cmdline/count_lines_argparse.py}
\end{frame}

\begin{frame}[fragile]
\frametitle{The basic elements of argparse}
\begin{itemize}
\item First import the module with \lstinline|import argparse|
\pause
\item Then get an argument parser. This is an object of type \lstinline|argparse.ArgumentParser| so we make one of those:
\begin{lstlisting}
parser = argparse.ArgumentParser(description='Some text to describe our program')
\end{lstlisting}
\pause
\item We add the descriptions of the arguments we need to parse with the \lstinline|add_argument| method of our parser object. This method has defaults for most of its parameters, but we need to supply at least an argument name, and it is a good idea to provide some help text to explain the use of the argument:
\begin{lstlisting}
parser.add_argument('some_name', help='Describe what some_name does')
\end{lstlisting}
\pause
\item Finally we tell the parser to process the argument list. By default this reads \lstinline|sys.argv| and it returns a namespace object that we can use to get at the argument values.
\begin{lstlisting}
args = parser.parse_args()
print args.some_name
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{What argparse does right}
\begin{itemize}
\item The argparse module handles counting of arguments and printing a useful error message if the command line arguments are incorrect. 
\pause
\item In addition to strings, it can process command line arguments as integers, file and boolean types.
\pause
\item It can handle both options (e.g. \textbf{-l}) and positional arguments.
\pause
\item Finally, \lstinline|argparse| can also handle optional arguments.
\pause
\item Read about \lstinline|argparse| at this \href{http://docs.python.org/2/howto/argparse.html}{tutorial} and the \href{http://docs.python.org/2.7/library/argparse.html}{official documentation}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Handling non-string arguments}
\begin{itemize}
\item The \lstinline|add_argument| method of the \lstinline|argparse| takes an optional \lstinline|type| parameter to specify the type that the argument should be converted to:
\begin{lstlisting}
parser.add_argument('line_num', type=int, help='Line number to read from file')
\end{lstlisting}
\pause
\item The \lstinline|argparse.FileType| class is used to specify that the argument is the name of a file that should be opened:
\begin{lstlisting}
parser.add_argument('input_file', type=argparse.FileType(), help='Input file')
parser.add_argument('output_file', type=argparse.FileType('w'), help='Out file')
\end{lstlisting}
\pause
\item Boolean types can be used for options, using a combination of \lstinline|action| and \lstinline|default| parameters to \lstinline|add_argument|:
\begin{lstlisting}
parser.add_argument('--zero_based', action='store_true', default=False)
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Non-string arguments: an example}
\lstinputlisting[backgroundcolor=\color{codebg},title={read\_linenum.py},linewidth=1.0\textwidth]{src/cmdline/read_linenum.py}
\end{frame}

\begin{frame}[fragile]
\frametitle{Optional and list arguments}
\begin{itemize}
\item The \lstinline|nargs| parameter to \lstinline|add_argument| changes how words are taken from the command line and returned as arguments. 
\pause
\item If \lstinline|nargs| is a number, a list is returned:
\begin{lstlisting}
parser.add_argument('read_files', nargs=2, type=argparse.FileType(), help='...')
\end{lstlisting}
\pause
\item If \lstinline|nargs| is `?', the argument is optional. This can be used to allow optional input or output files:
\begin{lstlisting}
parser.add_argument('out_file', nargs='?', type=argparse.FileType('w'),
					default=sys.stdout, help='Output file')
\end{lstlisting}
\pause
\item If \lstinline|nargs| is `*', all the remaining words are taken from the command line and returned as a list:
\begin{lstlisting}
parser.add_argument('in_files', nargs='*', type=argparse.FileType(),
				    help='Files to read')
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{List arguments: an example}
\lstinputlisting[backgroundcolor=\color{codebg},title={count\_files.py},linewidth=1.0\textwidth]{src/cmdline/count_files.py}

\end{frame}

\begin{frame}
\frametitle{In Conclusion}
\begin{itemize}
\item The \lstinline|sys.argv| list provides a list of command line arguments. The first element of the list is the name of the currently running program.
\pause
\item For all except the simplest cases, it is easier to use the \lstinline|argparse| module to parse (i.e. interpret) command line arguments.
\pause
\item By default, \lstinline|argparse| treats command line arguments as strings, but it can convert arguments into different types (such as int and file types).
\pause
\item Arguments whose names start with `-' and `--' are short and long options respectively. By contrast, other arguments are called positional arguments and are the `main' arguments of the program, whereas options modify how the positional arguments are interpreted.
\pause
\item Optional arguments and list arguments can be parsed using the \lstinline|nargs| parameter of the \lstinline|add_argument| method.
\end{itemize}
\end{frame}
\end{document}
